# Simple Blockchain

Una _BlockChain_ è una sequenza di oggetti detti "blocchi" dove ogni blocco contiene un hash crittografico calcolato a partire dai suoi dati e dall'hash del blocco precedente. Quindi, la sequenza non può essere manipolata senza invalidare gli hash dei blocchi modificati; in questo modo, i blocchi formano una "catena" difficile da modificare.

Per consentire ad agenti che non si conoscono o fidano l'uno dell'altro di aggiungere un blocco alla catena è necessario fornire una "prova" di avere dell'interesse o aver speso del lavoro per; il metodo utilizzato per questa "prova" definisce molte delle caratteristiche della blockchain. I più popolari metodi sono Proof of Work (usato da Bitcoin e molte altre criptovalute) e Proof of Stake (allo studio da parte di diversi concorrenti di Bitcoin). Il procedimento per aggiungere un blocco alla catena attraverso la fornitura di una prova è detto _mining_.

Una possibile implementazione di un Proof of Work è l'imposizione di una regola sul valore dell'hash del blocco che si intende aggiungere. Il "lavoro" che si deve quindi provare è la ricerca di un parametro dei dati del blocco tale per cui l'hash risultante dal blocco stesso soddisfa la regola. Questo significa essenzialmente effettuare una ricerca bruta sul dominio dei possibili valori del parametro e costituisce il "lavoro" di cui si dà "prova".

## pcd2017.exer2.Block

Questa classe rappresenta un blocco di una blockchain. I suoi campi sono:

* **data**: dati trasportati dal blocco, composti a loro volta da timestamp, hash del blocco precedente, e dati effettivamente trasportati.
* **hash**: hash del blocco stesso, costruito a partire dalla canonicalizzazione dei dati e del parametro
* **nonce**: parametro del blocco scoperto tramite il mining (il termine deriva dalla terminologia crittografica).

L'attività di mining consiste nel trovare, dato un oggetto di dati che si vuole aggiungere alla catena, il _nonce_ che rende l'hash risultante conforme alle regole della blockchain.
La regola di appartenenza è che l'hash debba iniziare con il prefisso dato dalla costante **HASH_PREFIX**. La lunghezza del prefisso è direttamente collegata alla difficoltà del problema (ed alla probabilità che il nonce sia effettivamente unico per un blocco) e quindi al tempo di calcolo necessario per risolverlo.

## pcd2017.exer2.Miner

Questa è la classe oggetto dell'esercizio. Rappresenta un processo che effettua il _mining_, cioè calcola il _nonce_ di un nuovo blocco che si desidera aggiungere alla catena trovando fra tutti i possibili valori quello che genera un hash che soddisfa le condizioni della catena.

I metodi già disponibili sono:

* **canonical**: calcola la stringa canonica dei dati e del nonce per calcolare l'hash. La stringa risultante va passata al metodo di hashing per ottenere l'hash dei dati.
* **sha256**: calcola l'hash dell'input e ne ritorna la sua rappresentazione in stringa esadecimale.
* **isTarget**: ritorna vero se l'hash soddisfa le condizioni di appartenenza alla blockchain
* **validate**: ritorna vero se il blocco è valido, cioè se l'hash soddisfa la condizione ed il valore è corretto per i dati che contiene.

## Tema dell'esercizio

L'esercizio prevede quattro compiti:

* preparare correttamente una fork **privata** del presente repository, un branch denominato secondo la propria matricola, ed una Pull Request verso il branch master del repository originale con il codice proposto come soluzione.
* configurare correttamente le pipeline sul proprio repository in modo che i test vengano eseguiti ad ogni commit e la PR sia correttamente marcata come corretta 
* implementare almeno uno dei metodi `Miner#threadMine(BlockData)` e `Miner#streamMine(BlockData)` che effettuano il lavoro di mining rispettivamente con uno stream parallelo e con più thread. Il risultato deve soddisfare i test `MinerTest#testStream()` e `MinerTest#testThread`.
* annotare sul file `ANSWER.md` la propria osservazione sulle differenze osservate fra i due metodi. Alcune possibili osservazioni sono: "quale dei due codici appare più semplice da leggere?"; "quale dei due codici sembra meglio occupare le CPU disponibili?"

### Punti di attenzione:

* la classe `MinerTest` non deve essere modificata.
* la classe `Miner` va modificata solo nei metodi sopra descritti. E' consentito aggiungere nello stesso file altre classi a supporto della soluzione. Non vanno modificati i metodi forniti. Il prefisso **HASH_PREFIX** va riconsegnato pari al suo valore originale.
* è consentito aggiungere eccezioni alla firma dei metodi `Miner#threadMine(BlockData)` e `Miner#streamMine(BlockData)`.
* la classe `Candidate` è un suggerimento per il suo uso come elemento dello stream di calcolo in `Miner#streamMine(BlockData)`.
* è possibile, se algoritmo stream-based e algoritmo thread-based percorrono lo spazio del parametro in ordine differente, che trovino _nonce_ differenti. Non è un problema, l'importante è che il test passi (la significatività di questo evento nell'ambito di una blockchain è un argomento da approfondire a parte).
* la complessità del processo di mining è direttamente correlata alla lunghezza del prefisso **HASH_PREFIX**. Per ottenere informazioni su cui basare il contenuto di `ANSWER.md` può essere utile sperimentare allungandolo o accorciandolo, ma va riconsegnato nella Pull Request non modificato.

# Riferimenti

* Bitbucket Pipelines: https://confluence.atlassian.com/bitbucket/build-test-and-deploy-with-pipelines-792496469.html
